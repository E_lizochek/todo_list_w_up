import "../ToDoForm/ToDoForm.scss"
import { Input } from "../Input/Input"
import { Button } from "../Button/Button"
import { ToDoList } from "../ToDoList/ToDoList"
import { useState } from 'react'




export function ToDoForm() {
    const [userInput, setUserInput] = useState('')
    const [tasks, setTasks] = useState([])


    const addTask = (userInput) => {
        if (userInput) {
            const newTask = {
                id: Math.floor(Math.random() * 1000).toString(36),
                task: userInput,
                complete: false
            }
            setTasks([...tasks, newTask])
        }
    }

    const handleChange = (element) => { setUserInput(element) }
    const handleSubmit = () => {
        addTask(userInput)
        setUserInput("")
    }
    return (
        <div className="todoform">
            <p> Available tasks: {tasks.length}</p>
            <div>
                <Input
                    type='text'
                    value={userInput}
                    placeholder="Input your text"
                    onChange={handleChange} />
                <Button
                    onClick={handleSubmit}
                    title="Add task" />
            </div>
            <ToDoList data={tasks} setTasks={setTasks} />
        </div>
    )
}