import {Input} from "../Input/Input"
import {Button} from "../Button/Button"
import {useState} from "react";
import "./FileUploader.scss"
import axios from "axios";

export function FileUploader () {
    const [file, setFile] = useState([])
    const handleChange = (files) => { 
        console.log(files)
        setFile(files[0]) }
const onSubmit=(e)=>{
    e.preventDefault();
    const data= new FormData();
data.append('file',file)
axios.post(`//localhost:3001/upload`,data)
.then((e)=>{
    console.log("Success")
    .catch((e)=>{
        console.error("Error",e)
    })
})
}
    return(
        <form method="post" action="#" id="#" onSubmit={onSubmit}>
            <div className="fileloader">
                <Input
                className="load"
                name="files[]"
                type="file"
                placeholder="Add your image"
                onChange={handleChange}
                multiple=""/>
                <Button
                theme="load"
                title=""
                onClick={()=>{}}
                />

            </div>
        </form>
    )
}

/* name
: 
"958834ccb34d8e68.webp"
size
: 
26206
type
: 
"image/webp"
webkitRelativePath
: 
"" */