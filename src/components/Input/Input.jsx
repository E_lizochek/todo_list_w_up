import '../Input/Input.scss'


export function Input(props){

    function handleChange(e) {
        props.onChange(e.currentTarget.value) || props.onChange(e.currentTarget.files[0])
    }

    return (
        <input 
        className='input' 
        name={props.name} 
        type={props.type} 
        placeholder={props.placeholder} 
        onChange ={handleChange} 
        multiple={props.multiple}/>
    )
}
