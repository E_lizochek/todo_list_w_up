import '../Button/Button.scss'
import classNames from 'classnames'

export function Button (props) {
    const ButtonClass = classNames (
        'button',
        { 'button__filter': props.theme === 'filter' },
        { 'button__done': props.theme === 'done' },
        { 'button__load': props.theme === 'load' },
        {'button__delete': props.theme === 'delete'})
        return (
            <button 
            className={ButtonClass} 
            onClick={props.onClick} 
            >{props.title}</button>
          );

}