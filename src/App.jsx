import './App.scss';
import { ToDoForm } from '../src/components/ToDoForm/ToDoForm'

function App() {
  return (
    <div className="App">
      <h1> ToDo list</h1>
      <ToDoForm />
    </div>
  );
}

export default App;
