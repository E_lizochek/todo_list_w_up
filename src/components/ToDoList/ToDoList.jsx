import '../ToDoList/ToDoList.scss'
import { Button } from "../Button/Button"
import { useState, useEffect } from "react"
import { FileUploader } from '../FileUploader/FileUploader'



export function ToDoList(props){
    const [filtered, setFiltered] = useState(props.data)

    const removeTask = (id) => {
        props.setTasks([...props.data.filter((task) => task.id !== id)])
    }

    const handleToggle = (id) => {
        props.setTasks([...props.data.map((task) =>
            task.id === id ? { ...task, complete: !task.complete } : { ...task }
        )])
    }

    function filterTask(complete) {
        if (complete === "all") {
            setFiltered(props.data)
        }
        else {
            let newTasks = [...props.data].filter(item => item.complete === complete)
            setFiltered(newTasks)
        }
    }

    useEffect(() => { setFiltered(props.data) }, [props.data])

    return (
        <>
            <div className='todolist'>
                <div className='img'></div>

                <Button theme='filter' title="all" onClick={() => filterTask('all')} />
                <Button theme='filter' title="done" onClick={() => filterTask(true)} />
                <Button theme='filter' title="new" onClick={() => filterTask(false)} />
            </div>
            {filtered.map((item) => {
                return (
                    <div className='todolist' key={item.id}>
                        <div className={item.complete ? "item-text strike" : "item-text"}> {item.task} </div>
                        <div className='buttondiv'>
                            <Button
                                theme='done'
                                title=""
                                onClick={() => handleToggle(item.id)} />
                            <Button
                                theme='delete'
                                title=""
                                onClick={() => removeTask(item.id)} />
                        </div>
                        <div>   
                            <FileUploader/></div>
                    </div>
                )
            })}
        </>
    )
}
